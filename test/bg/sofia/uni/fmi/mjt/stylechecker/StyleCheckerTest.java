package bg.sofia.uni.fmi.mjt.stylechecker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class StyleCheckerTest {

    private final static String NEW_LINE = System.lineSeparator();
    private final static String WILDCARDS_NOT_ALLOWED = "// FIXME Wildcards are not allowed in import statements";
    private final static String ONE_STATEMENT_PER_LINE = "// FIXME Only one statement per line is allowed";
    private final static String LINE_LENGTH = "// FIXME Length of line should not exceed 100 characters";
    private final static String SAME_LINE_BRACKETS = "// FIXME Opening brackets should be placed on the same line as the declaration";
    private final static String PACKAGE_NAME_FORMAT = "// FIXME Package name should not contain upper-case letters or underscores";

    private StyleChecker checker = StyleChecker.getInstance();
    private Reader input;
    private Writer output;

    @Before
    public void init() {
        output = new StringWriter();
    }

    @After
    public void close() throws IOException {
        input.close();
        output.close();
    }

    @Test
    public void testImportWildcardsRule() throws IOException {
        String lineToTest = "import java.util.*;";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String actual = output.toString();
        String expected = WILDCARDS_NOT_ALLOWED + NEW_LINE + lineToTest + NEW_LINE;

        //FAILURE MESSAGES ARE NOT NECESSARY AS THE TEST NAME IS VERY MUCH SELF EXPLANATORY
        assertEquals(expected, actual);
    }

    @Test
    public void testImportWithLeadingWhitespaces() throws IOException {
        String lineToTest = "         import java.util;";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String actual = output.toString();
        String expected = lineToTest + NEW_LINE;

        assertEquals(expected, actual);
    }

    @Test
    public void testLineLength() throws IOException {
        String lineToTest = "String expected = \"FIXME Wildcards are not allowed in import more text here to make" +
                " this longer than 100 lines statements\" + NEW_LINE + \"import java.util.*\";";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = LINE_LENGTH + NEW_LINE + lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testLineLengthWithWhitespaces() throws IOException {
        String lineToTest = "                                     allowed in import more text here to make" +
                " this long                                                                   ";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testLineLengthWithImport() throws IOException {
        String lineToTest = "import java.util.superlongline.shouldvalidatedespitethefactitswaytoolongforgoodquality" +
                "code.andisverylong.butstillanimportsoletssseewhathappensnext.shallwe.util.array.map.something" +
                "thislineis.obviouslywaytoolongtobeconsideredappropriate";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testForbiddenCharactersInPackageWithUnderscore() throws IOException {
        String lineToTest = "package java.util._blep;";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = PACKAGE_NAME_FORMAT + NEW_LINE + lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testForbiddenCharactersInPackageWithCapitalLetter() throws IOException {
        String lineToTest = "package java.util.BLep;";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = PACKAGE_NAME_FORMAT + NEW_LINE + lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testOneStatementPerLine() throws IOException {
        String testLine = "sayHello();sayHello();";
        input = new StringReader(testLine);
        checker.checkStyle(input, output);

        String expected = ONE_STATEMENT_PER_LINE + NEW_LINE + testLine + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testOneStatementPerLineWithMultipleSemicolons() throws IOException {
        String testLine = "sayHello();;;;";
        input = new StringReader(testLine);
        checker.checkStyle(input, output);

        String expected = testLine + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testBracketOnNewLine() throws IOException {
        String testLine = "{ sayHello();";
        input = new StringReader(testLine);
        checker.checkStyle(input, output);

        String expected = SAME_LINE_BRACKETS + NEW_LINE + testLine + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testValidLine() throws IOException {
        String testLine = "Reader input = new StringReader(testLine);";
        input = new StringReader(testLine);
        checker.checkStyle(input, output);

        String expected = testLine + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testMultipleProblemsInOneLine() throws IOException {
        String lineToTest = "import java.util.*; import java.util.boom;";
        input = new StringReader(lineToTest);
        checker.checkStyle(input, output);

        String expected = WILDCARDS_NOT_ALLOWED + NEW_LINE + ONE_STATEMENT_PER_LINE + NEW_LINE + lineToTest + NEW_LINE;
        String actual = output.toString();

        assertEquals(expected, actual);
    }
}
