package bg.sofia.uni.fmi.mjt.stylechecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Checks adherence to Java style guidelines.
 */
public class StyleChecker {

    private static final String WILDCARDS_NOT_ALLOWED = "// FIXME Wildcards are not allowed in import statements";
    private static final String ONE_STATEMENT_PER_LINE = "// FIXME Only one statement per line is allowed";
    private static final String LINE_LENGTH = "// FIXME Length of line should not exceed 100 characters";
    private static final String SAME_LINE_BRACKETS = "// FIXME Opening brackets should be placed on the same line as the declaration";
    private static final String PACKAGE_NAME_FORMAT = "// FIXME Package name should not contain upper-case letters or underscores";
    private static final String NEW_LINE = System.lineSeparator();
    private static final String ONE_STATEMENT_PER_LINE_REGEX = ".+;[^;]+;";
    private static final String CAPITAL_LETTER_REGEX = ".*[A-Z].*";
    private static final String IMPORT_LINE_START = "import";
    private static final String IMPORT_WILDCARD = "*";
    private static final String PACKAGE_LINE_START = "package";
    private static final String CURLY_BRACKET = "{";
    private static final String UNDERSCORE = "_";
    private final static int MAX_LINE_LENGTH = 100;

    private static StyleChecker styleCheckerInstance = null;

    private StyleChecker() {
    }

    //THIS COULD BE SYNCHRONIZED IF IT NEEDS TO BE THREAD SAFE
    public static StyleChecker getInstance() {

        if (styleCheckerInstance == null) {
            styleCheckerInstance = new StyleChecker();
        }

        return styleCheckerInstance;
    }

    /**
     * For each line from the given {@code source} performs code style checks
     * and writes to the {@code output}
     * 1. a FIXME comment line for each style violation in the input line, if any
     * 2. the input line itself.
     *
     * @param source
     * @param output
     */
    public void checkStyle(Reader source, Writer output) throws IOException {
        //A VALIDATOR CLASS COULD BE CREATED WHICH VALIDATES A LINE BASED ON RULES, BUT IS COMPLETE OVERKILL FOR THIS TASK
        BufferedReader bufferedReader = new BufferedReader(source);
        StringBuilder content = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            String trimmedLine = line.trim();

            boolean isImport = trimmedLine.startsWith(IMPORT_LINE_START);

            if (!isImport && trimmedLine.length() > MAX_LINE_LENGTH) {
                content.append(LINE_LENGTH).append(NEW_LINE);
            }

            if (isImport && trimmedLine.contains(IMPORT_WILDCARD)) {
                content.append(WILDCARDS_NOT_ALLOWED).append(NEW_LINE);
            }

            if (trimmedLine.matches(ONE_STATEMENT_PER_LINE_REGEX)) {
                content.append(ONE_STATEMENT_PER_LINE).append(NEW_LINE);
            }

            if (trimmedLine.startsWith(CURLY_BRACKET)) {
                content.append(SAME_LINE_BRACKETS).append(NEW_LINE);
            } else if (trimmedLine.startsWith(PACKAGE_LINE_START)
                    && (trimmedLine.matches(CAPITAL_LETTER_REGEX) || trimmedLine.contains(UNDERSCORE))) {
                content.append(PACKAGE_NAME_FORMAT).append(NEW_LINE);
            }

            content.append(line).append(NEW_LINE);
        }

        output.append(content);
        bufferedReader.close();
    }
}
